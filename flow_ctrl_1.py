

ip1="192.168.0.0.1"
ip2="192.168.256.1"
ip3="192.168.0.1"


def print_ip(ip):
    ip_list=ip.split('.')

    if(len(ip_list)!=4):
        print("ERROR:there must be 4 bytes in ip addres")
    else:
        for byte in ip_list:
            if(int(byte)>255):
                print("ERROR:byte cannot be greater than 255")
            else:
                print(byte)
    return 


print(ip1)
print_ip(ip1)

print('\n'+ip2)
print_ip(ip2)

print('\n'+ip3)
print_ip(ip3)
