def print_ip_bytes(ip):
    ip_list=ip.split('.')

    if(len(ip_list)!=4):
        print("ERROR:there must be 4 bytes in ip addres")
    else:
        for byte in ip_list:
            if(int(byte)>255):
                print("ERROR:byte cannot be greater than 255")
            else:
                print(byte)
    return


